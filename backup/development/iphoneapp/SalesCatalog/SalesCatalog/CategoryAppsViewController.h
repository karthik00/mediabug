//
//  FeaturedApplications.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Application.h"
#import "CategoryServeProtocol.h"
#import "ApplicationServeProtocol.h"

@interface CategoryAppsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ApplicationServeProtocol>{
    NSArray * applications;
    Application * currentSelectedApplication;
    UIViewController * detailedViewController;
    UITableView *tableView;
    id<CategoryServeProtocol> delegate;
}

@property (retain, nonatomic) id<CategoryServeProtocol> delegate;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (retain, nonatomic) NSArray * applications;
@property (retain, nonatomic) Application * currentSelectedApplication;
@property (retain, nonatomic) UIViewController * detailedViewController;

@end
