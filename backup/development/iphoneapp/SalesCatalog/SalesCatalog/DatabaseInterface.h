//
//  DatabaseInterface.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseInterface : NSObject

+(NSManagedObjectContext *)managedObject;
-(void)syncData:(NSDictionary *)syncDict;


+(DatabaseInterface *)sharedInstance;

@end
