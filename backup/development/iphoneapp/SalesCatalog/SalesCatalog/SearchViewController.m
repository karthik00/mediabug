//
//  Class.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SearchViewController.h"
#import "ApplicationDetailedViewcontroller.h"
#import "ApplicationCell.h"
#import "ServerInterface.h"
#import "Category.h"

@interface SearchViewController (private)
-(void)fetchResults;
@end

@implementation SearchViewController

@synthesize searchBar;
@synthesize tableView;
@synthesize applications;
@synthesize currentSelectedApplication;
@synthesize detailedViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [self setSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.searchBar resignFirstResponder];
}

-(void)loadData{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:[DatabaseInterface managedObject]];

    [request setPredicate:[NSPredicate predicateWithFormat:@"name contains[cd] %@",searchBar.text]];
    [request setEntity:entity];NSError *error = nil;
    
    self.applications= [[[DatabaseInterface managedObject] executeFetchRequest:request error:&error] mutableCopy];
    
    if (self.applications == nil) {
        // Handle the error.
    }
    [self.tableView reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self loadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)ksearchBar{
    [ksearchBar resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)ksearchBar{
    [ksearchBar resignFirstResponder];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.currentSelectedApplication=[applications objectAtIndex:[indexPath row]];
    ApplicationDetailedViewcontroller * tmp=[[ApplicationDetailedViewcontroller alloc] initWithNibName:@"ApplicationDetail" bundle:nil];
    tmp.delegate=self;
    self.detailedViewController=tmp;
    self.detailedViewController.hidesBottomBarWhenPushed=YES;
    [tmp release];
    [self.navigationController pushViewController:self.detailedViewController animated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.applications count];
}

- (UITableViewCell *)tableView:(UITableView *)ktableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{ 
    ApplicationCell * cell=(ApplicationCell *)[ktableView dequeueReusableCellWithIdentifier:@"application_cell"];
    if (!cell) {
        cell=(ApplicationCell *)[[[NSBundle mainBundle] loadNibNamed:@"application_cell" owner:self options:nil] objectAtIndex:0];
    }
    Application * app=[applications objectAtIndex:indexPath.row];
    cell.application_name_label.text=app.name;
    cell.category_label.text=app.category.name;
    [cell.backgroundView setBackgroundColor:[indexPath row]%2==0?[UIColor lightGrayColor]:[UIColor grayColor]];
    [app retriveApplicationIconWithDataReciever:cell];
    return cell;
}


#pragma URLCALLS

-(void)fetchResults{
    NSAutoreleasePool * pool= [[NSAutoreleasePool alloc] init];
    if ([searchBar.text compare:@""]!=0) {
        //ServerInterface * instance=[ServerInterface sharedInstance];
        //[instance serchForApplicationsWithQuery:searchBar.text withCallback:@selector(fetchResultsCallback:) andCallbackDelegate:self];
        //    [instance 
        //   fetchFeaturedApplicationsWithcallBack:@selector(fetchResultsCallback:) andCallbackDelegate:self];
    }
    [pool release];
}

-(void)fetchResultsCallback:(id)message{
    NSArray * result=(NSArray *)message;
    NSLog(@"hello %@",result);
   // NSArray * resultObjects=[Application objectsFromArray:result];
    //self.applications=resultObjects;
    [self.tableView reloadData];
}


-(Application *)currentApplication{
    return self.currentSelectedApplication;
}

- (void)dealloc {
    [searchBar release];
    [super dealloc];
}
@end
