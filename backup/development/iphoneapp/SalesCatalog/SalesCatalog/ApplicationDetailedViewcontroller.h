//
//  ApplicationDetailedViewcontroller.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationServeProtocol.h"
#import "AsyncDataReceiver.h"

@interface ApplicationDetailedViewcontroller : UIViewController <AsyncDataReceiver>{
    id<ApplicationServeProtocol> _delegate;
    UILabel *application_name_label;
    IBOutlet UIScrollView *mInfoScrollView;
    UILabel *category_label;
    UIImageView *app_icon;
    IBOutlet UIScrollView *mScreenView;
    UITextView *application_info_text_view;
    IBOutlet UIWebView *mInfoView;
}

@property (nonatomic, retain) IBOutlet UILabel *application_name_label;
@property (nonatomic, retain) IBOutlet UILabel *category_label;
@property (nonatomic, retain) IBOutlet UIImageView *app_icon;
- (IBAction)installApplication:(id)sender;

@property (retain, nonatomic) id<ApplicationServeProtocol> delegate;

@end