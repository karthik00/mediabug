//
//  ApplicationServeProtocol.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Category.h"

@protocol CategoryServeProtocol <NSObject>
@required
-(Category *)currentCategory;
@end