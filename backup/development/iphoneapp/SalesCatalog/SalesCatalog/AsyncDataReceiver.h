//
//  AsyncDataReciever.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AsyncDataReceiver <NSObject>
@required
-(void)setAsyncDataWithObject:(id)application;
@end