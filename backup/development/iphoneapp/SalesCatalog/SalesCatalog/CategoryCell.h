//
//  CategoryCell.h
//  sales_lab
//
//  Created by Sourcebits on 11/18/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UITableViewCell
{
    UILabel *category_name;
    UIImageView *category_icon;
    UILabel *category_sub;

}
@property (nonatomic, retain) IBOutlet UILabel *category_name;
@property (nonatomic, retain) IBOutlet UIImageView *category_icon;
@property (nonatomic, retain) IBOutlet UILabel *category_sub;
@end
