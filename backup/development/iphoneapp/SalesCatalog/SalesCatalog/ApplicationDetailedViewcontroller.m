//
//  ApplicationDetailedViewcontroller.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ApplicationDetailedViewcontroller.h"
#import "ScreenShot.h"
#import "Category.h"

@interface ApplicationDetailedViewcontroller (private) 
-(void)loadContents;
-(void)loadScreenShots;
@end

@implementation ApplicationDetailedViewcontroller
//@synthesize application_info_text_view;
@synthesize application_name_label;
@synthesize category_label;
@synthesize app_icon;
@synthesize delegate=_delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    mInfoScrollView.contentSize=CGSizeMake(320, 480);
    mScreenView.contentSize=CGSizeMake(350,480);
    [self loadContents];
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadContents];
}

-(void)loadContents{
    self.title=[[self.delegate currentApplication] name];
    self.application_name_label.text=[[self.delegate currentApplication] name];
    self.category_label.text=[[[self.delegate currentApplication] category] name];
    [mInfoView loadHTMLString:[[self.delegate currentApplication] info] baseURL:nil];
    [[self.delegate currentApplication] retriveApplicationIconWithDataReciever:self];
    [self loadScreenShots];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *output = [mInfoView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
    NSLog(@"height: %@", output);
    CGRect tmp= mInfoView.frame;
    tmp.size.height=[output floatValue];
    mInfoView.frame=tmp;
    mInfoScrollView.contentSize=CGSizeMake(mInfoScrollView.frame.size.width, [output floatValue]+260);
    mScreenView.frame=CGRectMake(0, mInfoView.frame.origin.x+[output floatValue], 320, 250);
}


-(void)loadScreenShots{
//    NSSet * screen_shots=[[self.delegate currentApplication] screen_shots];
//    for (ScreenShot * screen_shot in screen_shots) {
//        ScreenShot * screenShot=[[ScreenShot alloc] initWithFrame:CGRectMake(170*[urls indexOfObject:url]+10, 10, 160, 240)];
//        screenShot.url=url;
//        [mScreenView addSubview:screenShot];
//        [[self.delegate currentApplication] retriveApplicationScreenShotWithDataReciever:screenShot andIndex:[urls indexOfObject:url]];
//        [screenShot release];
//    }
//    mScreenView.contentSize=CGSizeMake(170*[urls count]+10,mScreenView.frame.size.height);
}

- (void)viewDidUnload{
    [self setApplication_name_label:nil];
    [self setCategory_label:nil];
    
    [mInfoView release];
    mInfoView = nil;
    [mInfoScrollView release];
    mInfoScrollView = nil;
   

    [mScreenView release];
    mScreenView = nil;
    [self setApp_icon:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [application_name_label release];
    [category_label release];
    [application_info_text_view release];
    [mInfoView release];
    [mInfoScrollView release];
    [mScreenView release];
    [super dealloc];
}

- (IBAction)installApplication:(id)sender {
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-services://?action=download-manifest&url=%@", [[self.delegate currentApplication] maifest_url]]
//                                                ]];
}

-(void)setAsyncDataWithObject:(Application *)application{
    self.app_icon.image=[UIImage imageWithData:application.icon];
}

@end
