//
//  ApplicationCell.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncDataReceiver.h"

@interface ApplicationCell : UITableViewCell <AsyncDataReceiver>{
    UILabel *application_name_label;
    UILabel *category_label;
    UIImageView *app_icon;
}

@property (nonatomic, retain) IBOutlet UILabel *application_name_label;
@property (nonatomic, retain) IBOutlet UILabel *category_label;
@property (nonatomic, retain) IBOutlet UIImageView *app_icon;

@end