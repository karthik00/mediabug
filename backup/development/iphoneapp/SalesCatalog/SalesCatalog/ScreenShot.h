//
//  ScreenShot.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ScreenShot : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSManagedObject *applciation;

-(id)initWithUrl:(NSString *)url;

@end
