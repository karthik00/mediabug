//
//  RecentApplicationsViewController.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 29/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationServeProtocol.h"

@interface RecentApplicationsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,ApplicationServeProtocol>{
    NSMutableArray * applications;
    Application * currentSelectedApplication;
    UIViewController * detailedViewController;
    UITableView *tableView;
}


@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (retain, nonatomic) NSMutableArray * applications;
@property (retain, nonatomic) Application * currentSelectedApplication;
@property (retain, nonatomic) UIViewController * detailedViewController;

@end
