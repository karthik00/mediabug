//
//  RecentApplicationsViewController.m
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 29/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RecentApplicationsViewController.h"
#import "ApplicationDetailedViewcontroller.h"
#import "ApplicationCell.h"
#import "Category.h"

@implementation RecentApplicationsViewController


@synthesize tableView;
@synthesize applications;
@synthesize currentSelectedApplication;
@synthesize detailedViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    self.tableView=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)loadData{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:[DatabaseInterface managedObject]];
    [request setEntity:entity];NSError *error = nil;
    [request setFetchLimit:20];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"updated_date" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [request setSortDescriptors:sortDescriptors];
    self.applications= [[[DatabaseInterface managedObject] executeFetchRequest:request error:&error] mutableCopy];
    if (self.applications == nil) {
        // Handle the error.
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadData];
    [tableView reloadData];
}

#pragma Tabledelegate and datasource

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.currentSelectedApplication=[applications objectAtIndex:[indexPath row]];
    ApplicationDetailedViewcontroller * tmp=[[ApplicationDetailedViewcontroller alloc] initWithNibName:@"ApplicationDetail" bundle:nil];
    tmp.delegate=self;
    self.detailedViewController=tmp;
    self.detailedViewController.hidesBottomBarWhenPushed=YES;
    [tmp release];
    [self.navigationController pushViewController:self.detailedViewController animated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.applications count];
}

- (UITableViewCell *)tableView:(UITableView *)ktableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{ 
    ApplicationCell * cell=(ApplicationCell *)[ktableView dequeueReusableCellWithIdentifier:@"application_cell"];
    if (!cell) {
        cell=(ApplicationCell *)[[[NSBundle mainBundle] loadNibNamed:@"application_cell" owner:self options:nil] objectAtIndex:0];
    }
    Application * app=[applications objectAtIndex:indexPath.row];
    cell.application_name_label.text=app.name;
    cell.category_label.text=app.category.name;
    [cell.backgroundView setBackgroundColor:[indexPath row]%2==0?[UIColor lightGrayColor]:[UIColor grayColor]];
    [app retriveApplicationIconWithDataReciever:cell];
    return cell;
}


-(Application *)currentApplication{
    return self.currentSelectedApplication;
}

@end
