//
//  Icon.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 24/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Icon.h"
#import <QuartzCore/QuartzCore.h>

@implementation Icon

- (id)init
{
    self = [super init];
    if (self) {
        self.layer.cornerRadius=12.0;
        self.clipsToBounds=true;
        self.layer.borderColor = [[UIColor grayColor] CGColor];
        self.layer.borderWidth = 1;
    }
    
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    
    self=[super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius=12.0;
        self.clipsToBounds=true;
        self.layer.borderColor = [[UIColor grayColor] CGColor];
        self.layer.borderWidth = 1;  
    }
    return self;
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        self.layer.cornerRadius=12.0;
        self.clipsToBounds=true;
        self.layer.borderColor = [[UIColor grayColor] CGColor];
        self.layer.borderWidth = 1;   
    }
    return self;
}

@end
