//
//  Build.m
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Build.h"
#import "Application.h"


@implementation Build
@dynamic version;
@dynamic id;
@dynamic application;

-(id)initWithDict:(NSDictionary *)dict andApplication:(Application *) application{
    self=[super init];
    if (self) {
        self.version=[dict objectForKey:@"version"];
        self.id=[dict objectForKey:@"id"];
        self.application=application;
    }
    return self;
}

@end
