//
//  CategoryViewController.m
//  sales_lab
//
//  Created by Sourcebits on 11/18/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CategoryViewController.h"
#import "ApplicationCell.h"
#import "Application.h"
#import "CategoryCell.h"
#import "Category.h"
#import "CategoryAppsViewController.h"

@implementation CategoryViewController

@synthesize tableView;
@synthesize categories;
@synthesize currentSelectedCategory;
@synthesize categoryAppsViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
   // [mCategoryList release];
    //mCategoryList = nil;
    self.tableView=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    //[mCategoryList release];
    [super dealloc];
}

-(Category *)currentCategory{
   return self.currentSelectedCategory;
}

-(void)loadData{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:[DatabaseInterface managedObject]];
    [request setEntity:entity];
    NSError *error = nil;
    
    self.categories= [[[DatabaseInterface managedObject] executeFetchRequest:request error:&error] mutableCopy];
    
    if (self.categories == nil) {
        // Handle the error.
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadData];
    //[tableView reloadData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.categories.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)ktableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{ 
    CategoryCell * cell=(CategoryCell *)[ktableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    if (!cell) {
        cell=(CategoryCell *)[[[NSBundle mainBundle] loadNibNamed:@"CategoryCell" owner:self options:nil] objectAtIndex:0];
    }
    Category * category=[self.categories objectAtIndex:indexPath.row];
    cell.category_name.text=category.name;
    switch (category.applciations.count) {
        case 0:
            cell.category_sub.text=@"No Apps";
            break;
        case 1:
            cell.category_sub.text=@"1 App";
            break;
        default:
            cell.category_sub.text=[NSString stringWithFormat:@"%i Apps", category.applciations.count];
            break;
    }
    [cell.backgroundView setBackgroundColor:[indexPath row]%2==0?[UIColor lightGrayColor]:[UIColor grayColor]];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.currentSelectedCategory=[self.categories objectAtIndex:[indexPath row]];
    CategoryAppsViewController * tmp=[[CategoryAppsViewController alloc] initWithNibName:@"CategoryApplicationsViewController" bundle:nil];
    self.categoryAppsViewController=tmp;
    self.categoryAppsViewController.delegate=self;
    self.categoryAppsViewController.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:self.categoryAppsViewController animated:YES];
    [tmp release];
}

@end
