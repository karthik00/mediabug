//
//  CategoryCell.m
//  sales_lab
//
//  Created by Sourcebits on 11/18/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell
@synthesize category_name;
@synthesize category_icon;
@synthesize category_sub;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)dealloc {
    [category_name release];
    [category_icon release];
    [category_sub release];
    [super dealloc];
}
@end
