//
//  Category.m
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Category.h"
#import "Application.h"


@implementation Category
@dynamic name;
@dynamic info;
@dynamic applciations;
@dynamic id;

-(void)updateWithDict:(NSDictionary *)dict{
        // {"created_at":"2011-11-28T15:26:00Z","description":"test","id":2,"name":"Test cat 2","updated_at":"2011-11-28T15:26:00Z"}
        self.name=[dict objectForKey:@"name"];
        self.info=[dict objectForKey:@"description"];
        self.id=[dict objectForKey:@"id"];
}

@end