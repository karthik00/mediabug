//
//  ScreenShot.m
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "ScreenShot.h"


@implementation ScreenShot
@dynamic url;
@dynamic image;
@dynamic applciation;

-(id)initWithUrl:(NSString *)url{
    self=[super init];
    if (self) {
        self.url=url;
    }
    return self;
}

@end
