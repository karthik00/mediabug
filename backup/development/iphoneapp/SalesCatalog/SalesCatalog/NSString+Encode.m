//
//  NSString+Encode.m
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 29/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSString+Encode.h"

@implementation NSString (encode)
- (NSString *)encodeString:(NSStringEncoding)encoding
{
    return (NSString *) CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                NULL, (CFStringRef)@";/?:@&=$+{}<>,",
                                                                CFStringConvertNSStringEncodingToEncoding(encoding));
}

@end