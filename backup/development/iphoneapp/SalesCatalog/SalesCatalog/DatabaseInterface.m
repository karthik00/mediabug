//
//  DatabaseInterface.m
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DatabaseInterface.h"
#import "Category.h"
#import "SalesCatalogAppDelegate.h"
#import "Application.h"

@implementation DatabaseInterface

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


+(DatabaseInterface *)sharedInstance{
    static DatabaseInterface * instance=nil;
    if(!instance){
        instance=[[DatabaseInterface alloc] init];
    }
    return instance;
}

+(NSManagedObjectContext *)managedObject{
    return [(SalesCatalogAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
}

+(void)saveUpdate{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:[NSDate date] forKey:@"last_update"];
}

-(void)syncData:(NSDictionary *)syncDict{
    
    NSManagedObjectContext *context = [(SalesCatalogAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    NSLog(@"test2");
    NSError * error;
    
    for (NSDictionary * dict in [syncDict objectForKey:@"added_categories"]) {
        Category *category = [NSEntityDescription
                              insertNewObjectForEntityForName:@"Category" 
                              inManagedObjectContext:context]; 
        [category updateWithDict:dict];
    }
    
    for (NSDictionary * dict in [syncDict objectForKey:@"updated_categories"]) {
        NSFetchRequest * fetch_request=[[NSFetchRequest alloc] init];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:context];
        [fetch_request setEntity:entityDescription]; 
        [fetch_request setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"id == '%@'",[dict objectForKey:@"id"]]]]; 
        Category * category=[[context executeFetchRequest:fetch_request error:&error] objectAtIndex:0];
        [fetch_request release];
        [category updateWithDict:dict];
    }
    
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    for (NSDictionary * dict in [syncDict objectForKey:@"added_applications"]) {
        
        Application *application = [NSEntityDescription insertNewObjectForEntityForName:@"Application" inManagedObjectContext:context];
        
        NSFetchRequest * fetch_request=[[NSFetchRequest alloc] init];
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:context];
        [fetch_request setEntity:entityDescription];
        [fetch_request setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"id == '%@'",[dict objectForKey:@"category_id"]]]];
        Category * category=[[context executeFetchRequest:fetch_request error:&error] objectAtIndex:0];
        
        [application updateWithDict:(NSDictionary *)dict category:category];
    }
    
    for (NSDictionary * dict in [syncDict objectForKey:@"updated_applications"]) {
        
        NSFetchRequest * application_fetch_request=[[NSFetchRequest alloc] init];
        NSEntityDescription * application_entityDescription = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:context];
        [application_fetch_request setEntity:application_entityDescription];
        [application_fetch_request setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"id == '%@'",[dict objectForKey:@"id"]]]]; 
        Application * application=[[context executeFetchRequest:application_fetch_request error:&error] objectAtIndex:0];
        [application_fetch_request release];
        
        if ([application.category.id isEqualToNumber:[dict objectForKey:@"category_id"]]) {
            [application updateWithDict:(NSDictionary *)dict category:application.category];
        }
        else{
            NSFetchRequest * fetch_request=[[NSFetchRequest alloc] init];
            NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:context];
            [fetch_request setEntity:entityDescription]; 
            [fetch_request setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"id == '%@'",[dict objectForKey:@"category_id"]]]]; 
            Category * category=[[context executeFetchRequest:fetch_request error:&error] objectAtIndex:0];
            [application updateWithDict:(NSDictionary *)dict category:category];
        }
        
    }
    
    if (![context save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
    
    for (NSDictionary * dict in [syncDict objectForKey:@"added_builds"]) {
        //add builds
    }
    
    for (NSDictionary * dict in [syncDict objectForKey:@"updated_builds"]) {
        //update builds
    }
    
    for (NSDictionary * dict in [syncDict objectForKey:@"deleted_items"]) {
        NSFetchRequest * fetch_request=[[NSFetchRequest alloc] init];
        NSEntityDescription *entityDescription;
        if ([(NSString *)[dict objectForKey:@"item_type"] compare:@"application"]==0) {
            entityDescription = [NSEntityDescription entityForName:@"Application" inManagedObjectContext:context];
        }
        else if([(NSString *)[dict objectForKey:@"item_type"] compare:@"category"]==0){
            entityDescription = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:context];
        }
        else if([(NSString *)[dict objectForKey:@"item_type"] compare:@"application_biuld"]==0){
            entityDescription = [NSEntityDescription entityForName:@"Build" inManagedObjectContext:context];
        }
        [fetch_request setEntity:entityDescription];
        [fetch_request setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"id == '%@'",[dict objectForKey:@"delete_id"]]]]; 
        [context deleteObject:[[context executeFetchRequest:fetch_request error:&error] objectAtIndex:0]];
    }
        
    [DatabaseInterface saveUpdate];
}

@end
