//
//  MainViewController.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UITabBarController{
    UIViewController * _loginViewController;
}

@property (retain, nonatomic) UIViewController * loginViewController;

@end