//
//  CategoryViewController.h
//  sales_lab
//
//  Created by Sourcebits on 11/18/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryServeProtocol.h"
#import "CategoryAppsViewController.h"

@interface CategoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, CategoryServeProtocol>
{
    NSMutableArray * categories;
    Category * currentSelectedCategory;
    CategoryAppsViewController * categoryAppsViewController;
}

@property (retain, nonatomic) NSMutableArray * categories;
@property (retain, nonatomic) Category * currentSelectedCategory;
@property (retain, nonatomic) IBOutlet UITableView * tableView;
@property (retain, nonatomic) CategoryAppsViewController * categoryAppsViewController;


@end
