//
//  Application.m
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "Application.h"
#import "Build.h"
#import "Category.h"
#import "ScreenShot.h"

@implementation Application
@dynamic name;
@dynamic info;
@dynamic icon;
@dynamic id;
@dynamic category;
@dynamic screen_shots;
@dynamic builds;
@dynamic icon_url;
@dynamic icon_downloaded;
@dynamic updated_date;

-(void)updateWithDict:(NSDictionary * )dict category:(Category *)category{
//{"applicationName":"Test Application","category":{"created_at":"2011-11-28T08:05:35Z","description":"test","id":1,"name":"Test Cat","updated_at":"2011-11-28T08:05:35Z"},"category_id":1,"created_at":"2011-11-28T08:08:03Z","icon_id":null,"id":1,"info":"<p>asdasd</p>","maifest_url":"asdasdasd","updated_at":"2011-11-28T08:08:03Z","snap_shot_urls":[],"icon_url":"/system/images/1/medium/IMG_0012.JPG?1322467683"}
        
        NSDateFormatter* df = [[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSString* str = [dict objectForKey:@"updated_at"];
        NSDate* date = [df dateFromString:str];
        self.updated_date=date;
        
        self.name=[dict objectForKey:@"applicationName"];
        self.category=category;
        self.info=[dict objectForKey:@"info"];
        self.id=[dict objectForKey:@"id"];
        if (self.icon_url) {
        if ([self.icon_url compare:[dict objectForKey:@"icon_url"]]!=0) {
                self.icon=nil;
                self.icon_downloaded=[NSNumber numberWithInt:0];
            }
        }
        else{
            self.icon_downloaded=[NSNumber numberWithInt:0];
        }
        self.icon_url=[dict objectForKey:@"icon_url"];
        for (NSString * url in [dict objectForKey:@"snap_shot_urls"]) {
            NSError * error;
            NSFetchRequest * fetch_request=[[NSFetchRequest alloc] init];
            NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"ScreenShot" inManagedObjectContext:self.managedObjectContext];
            [fetch_request setEntity:entityDescription];
            [fetch_request setPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"url == '%@'",[dict objectForKey:@"url"]]]];
            NSArray * result=[self.managedObjectContext executeFetchRequest:fetch_request error:&error];
            if ([result count]!=0) {
                ScreenShot *shot = [result objectAtIndex:0];
                shot.url=url;
                shot.applciation=self;
            }
            else{
                ScreenShot *shot = [NSEntityDescription
                                    insertNewObjectForEntityForName:@"ScreenShot" 
                                    inManagedObjectContext:self.managedObjectContext];
                shot.url=url;
                shot.applciation=self;
            }
        }
}




-(void)retriveApplicationIconWithDataReciever:(id)dataReciever{
    if ([self.icon_downloaded intValue]==1) {
        [dataReciever performSelectorOnMainThread:@selector(setAsyncDataWithObject:) withObject:self waitUntilDone:YES];
        return;
    }
    [NSThread detachNewThreadSelector:@selector(asyncLoadBinaryFileWithReciever:) toTarget:self withObject:[NSArray arrayWithObjects:dataReciever, self.icon_url, nil]];
}

-(void)asyncLoadBinaryFileWithReciever:(NSArray *)args{
    NSAutoreleasePool * pool= [[NSAutoreleasePool alloc] init];
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:URL_SLABS_FILE, self.icon_url]];
    NSURLResponse * response=nil;
    NSError * error=nil;
    NSData * data=[NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:&response error:&error];
    self.icon=data;
    self.icon_downloaded=[NSNumber numberWithInt:1];
    [self.managedObjectContext save:&error];
    [[args objectAtIndex:0] performSelectorOnMainThread:@selector(setAsyncDataWithObject:) withObject:self waitUntilDone:YES];
    [pool release];
}

@end
