//
//  FeaturedApplications.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CategoryAppsViewController.h"
#import "ServerInterface.h"
#import "Application.h"
#import "ApplicationDetailedViewcontroller.h"
#import "ApplicationCell.h"
#import "DatabaseInterface.h"
#import "Category.h"
#import "CategoryServeProtocol.h"

@interface CategoryAppsViewController(private)
-(void)loadData;
@end

@implementation CategoryAppsViewController

@synthesize tableView;
@synthesize applications;
@synthesize currentSelectedApplication;
@synthesize detailedViewController;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{   
    [super viewDidLoad];
    //[self loadData];
}

-(void)loadData{
    self.title=[[self.delegate currentCategory] name];
    self.applications= [[[self.delegate currentCategory] applciations] allObjects];
    if (self.applications == nil) {
        // Handle the error.
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self loadData];
    [tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
//    ServerInterface * instance=[ServerInterface sharedInstance];
//    if(instance.loggedIn){
//        [NSThread detachNewThreadSelector:@selector(fetchResults) toTarget:self withObject:nil];
//    }
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma Tabledelegate and datasource

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.currentSelectedApplication=[applications objectAtIndex:[indexPath row]];
        ApplicationDetailedViewcontroller * tmp=[[ApplicationDetailedViewcontroller alloc] initWithNibName:@"ApplicationDetail" bundle:nil];
        tmp.delegate=self;
        self.detailedViewController=tmp;
        self.detailedViewController.hidesBottomBarWhenPushed=YES;
        [tmp release];
    [self.navigationController pushViewController:self.detailedViewController animated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.applications count];
}

- (UITableViewCell *)tableView:(UITableView *)ktableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{ 
    ApplicationCell * cell=(ApplicationCell *)[ktableView dequeueReusableCellWithIdentifier:@"application_cell"];
    if (!cell) {
        cell=(ApplicationCell *)[[[NSBundle mainBundle] loadNibNamed:@"application_cell" owner:self options:nil] objectAtIndex:0];
    }
    Application * app=[applications objectAtIndex:indexPath.row];
    cell.application_name_label.text=app.name;
    cell.category_label.text=app.category.name;
    [cell.backgroundView setBackgroundColor:[indexPath row]%2==0?[UIColor lightGrayColor]:[UIColor grayColor]];
    [app retriveApplicationIconWithDataReciever:cell];
    return cell;
}


#pragma URLCALLS

-(void)fetchResults{
    NSAutoreleasePool * pool= [[NSAutoreleasePool alloc] init];
    ServerInterface * instance=[ServerInterface sharedInstance];
    [instance fetchFeaturedApplicationsWithcallBack:@selector(fetchResultsCallback:) andCallbackDelegate:self];
    [pool release];
}

-(void)fetchResultsCallback:(id)message{
    //NSArray * result=(NSArray *)message;
    //NSArray * resultObjects=[Application objectsFromArray:result];
    //self.applications=resultObjects;
    [self.tableView reloadData];
}

-(Application *)currentApplication{
    return self.currentSelectedApplication;
}

- (void)dealloc {
    [tableView release];
    [super dealloc];
}
@end