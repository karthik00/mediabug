//
//  Category.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Application;

@interface Category : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSSet *applciations;
@property (nonatomic, retain) NSNumber * id;
@end

@interface Category (CoreDataGeneratedAccessors)

- (void)addApplciationsObject:(Application *)value;
- (void)removeApplciationsObject:(Application *)value;
- (void)addApplciations:(NSSet *)values;
- (void)removeApplciations:(NSSet *)values;

-(void)updateWithDict:(NSDictionary *)dict;

@end
