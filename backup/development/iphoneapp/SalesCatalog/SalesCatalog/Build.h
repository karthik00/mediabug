//
//  Build.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Application;

@interface Build : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * version;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) Application *application;

@end
