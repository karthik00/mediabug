//
//  ApplicationCell.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ApplicationCell.h"
#import "Application.h"

@implementation ApplicationCell
@synthesize application_name_label;
@synthesize category_label;
@synthesize app_icon;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self=[super initWithCoder:aDecoder];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setAsyncDataWithObject:(Application *)application{
    self.app_icon.image=[UIImage imageWithData:application.icon];
}

- (void)dealloc {
    [application_name_label release];
    [category_label release];
    [app_icon release];
    [super dealloc];
}
@end
