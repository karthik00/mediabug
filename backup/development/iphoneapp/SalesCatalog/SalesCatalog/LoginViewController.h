//
//  LoginViewController.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController{
    UITextField *emailTextField;
    UITextField *passwordTextField;
}

@property (nonatomic, retain) IBOutlet UITextField *emailTextField;

@property (nonatomic, retain) IBOutlet UITextField *passwordTextField;


- (IBAction)login:(id)sender;

@end
