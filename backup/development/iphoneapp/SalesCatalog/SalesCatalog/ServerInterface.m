//
//  ServerInterface.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ServerInterface.h"
#import "DatabaseInterface.h"
#import "NSString+Encode.h"


@interface ServerInterface (private)
@end

@implementation ServerInterface

@synthesize loggedIn;
@synthesize decoder;
@synthesize userDict;

- (id)init
{
    self = [super init];
    if (self) {
        self.loggedIn=false;
        self.decoder=[JSONDecoder decoder];
    }
    return self;
}

-(void)loginUsingUserName:(NSString *) email andPassword:(NSString *) password callBack:(SEL)callback andCallbackDelegate :(id)delegate{
    if(!self.loggedIn){
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults]; 
        NSString *val = nil; 
        if (standardUserDefaults) 
            val = [[standardUserDefaults objectForKey:@"last_update"] description];
        NSURL * url;
        NSString *encodedStr;
        if (val) {
            NSString *encoded = (NSString *)CFURLCreateStringByAddingPercentEscapes(
                                                                                    NULL,
                                                                                    (CFStringRef)val,
                                                                                    NULL,
                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                    kCFStringEncodingUTF8
                                                                                    );
            
            encodedStr = [NSString stringWithFormat:URL_SLABS_LOGIN_PARAMS, email, password, encoded];
        }
        else{
            encodedStr = [NSString stringWithFormat:URL_SLABS_LOGIN_WITH_INITIAL_SYNC_PARAMS, email, password];
        }
        
        url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",URL_SLABS_LOGIN, encodedStr]];
        
        NSURLResponse * response;
        NSError * error;
        NSData * data=[NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:&response error:&error];
        id response_data=[decoder objectWithData:data];
        if (![response_data objectForKey:@"error"]) {
            [[DatabaseInterface sharedInstance] performSelectorOnMainThread:@selector(syncData:) withObject:[response_data objectForKey:@"sync_data"] waitUntilDone:YES];
        }
        NSLog(@"%@", response_data);
        [delegate performSelectorOnMainThread:callback withObject:response_data waitUntilDone:YES];
    }
}


-(void)fetchFeaturedApplicationsWithcallBack:(SEL)callback andCallbackDelegate :(id)delegate{
        NSURL * url=[NSURL URLWithString:[NSString stringWithFormat:URL_SLABS_GET_FEATURED_APPLICATIONS]];
        NSURLResponse * response;
        NSError * error;
        NSData * data=[NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:&response error:&error];
        id response_data=[decoder objectWithData:data];
        [delegate performSelectorOnMainThread:callback withObject:response_data waitUntilDone:YES];
}


//
//-(void)serchForApplicationsWithQuery:(NSString *)query withCallback:(SEL)callback andCallbackDelegate:(id)delegate{
//    NSURL * url=[NSURL URLWithString:[NSString stringWithFormat:URL_SLABS_SEARCH_APPS, query]];
//    NSURLResponse * response;
//    NSError * error;
//    NSData * data=[NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:&response error:&error];
//    id response_data=[decoder objectWithData:data];
//    [delegate performSelectorOnMainThread:callback withObject:response_data waitUntilDone:YES];
//}

-(void)setUserDict:(NSDictionary *)dict{
    if (userDict) {
        NSDictionary * temp=userDict;
        userDict=[dict retain];
        [temp release];
        self.loggedIn=TRUE;
    }
    else{
        userDict=[dict retain];
        self.loggedIn=TRUE;
    }
}


+(ServerInterface *)sharedInstance{
    static ServerInterface * instance=nil;
    if(!instance){
        instance=[[ServerInterface alloc] init];
    }
    return instance;
}

@end