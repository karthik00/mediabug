//
//  NSString+Encode.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 29/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
//  NSString+Encode.h

@interface NSString (encode)
- (NSString *)encodeString:(NSStringEncoding)encoding;
@end