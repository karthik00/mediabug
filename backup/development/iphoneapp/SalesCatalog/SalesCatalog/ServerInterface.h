//
//  ServerInterface.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONKit.h"


#define URL_SLABS_LOGIN @"http://61.12.14.178:4500/device/login_app_login.json"
#define URL_SLABS_LOGIN_PARAMS @"?email=%@&password=%@&from_date=%@"

#define URL_SLABS_LOGIN_WITH_INITIAL_SYNC @"http://61.12.14.178:4500/device/login_app_login.json"
#define URL_SLABS_LOGIN_WITH_INITIAL_SYNC_PARAMS @"?email=%@&password=%@"

#define URL_SLABS_GET_FEATURED_APPLICATIONS @"http://61.12.14.178:4500/applications.json"
#define URL_SLABS_SEARCH_APPS @"http://61.12.14.178:4500/search/%@.json"
#define URL_SLABS_FILE @"http://61.12.14.178:4500%@"


@interface ServerInterface : NSObject{
    BOOL loggedIn;
    JSONDecoder *decoder;
    NSDictionary * userDict;
}

@property (assign) BOOL loggedIn;
@property (retain, nonatomic) JSONDecoder * decoder;
@property (retain, nonatomic) NSDictionary * userDict;

+(ServerInterface *)sharedInstance;

-(void)loginUsingUserName:(NSString *) email andPassword:(NSString *) password callBack:(SEL)callback andCallbackDelegate :(id)delegate;

-(void)fetchFeaturedApplicationsWithcallBack:(SEL)callback andCallbackDelegate :(id)delegate;

@end
