//
//  LoginViewController.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"
#import "ServerInterface.h"
#import "DatabaseInterface.h"


@implementation LoginViewController

@synthesize emailTextField;
@synthesize passwordTextField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [self setEmailTextField:nil];
    [self setPasswordTextField:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [emailTextField release];
    [passwordTextField release];
    [super dealloc];
}
- (IBAction)login:(id)sender {
    [NSThread detachNewThreadSelector:@selector(login) toTarget:self withObject:nil];
}

-(void)login{
    NSAutoreleasePool * pool=[[NSAutoreleasePool alloc] init];
    ServerInterface * instance=[ServerInterface sharedInstance];
    [instance loginUsingUserName:self.emailTextField.text andPassword:self.passwordTextField.text callBack:@selector(loginCallback:) andCallbackDelegate:self];
    [pool release];
}

-(void)loginCallback:(id) message{
    NSDictionary * result=(NSDictionary *)message;
    if (![result objectForKey:@"error"]) {
        ServerInterface * instance=[ServerInterface sharedInstance];
        instance.userDict=[message objectForKey:@"user"];
        [self dismissModalViewControllerAnimated:YES];
    }
    else{
        UIAlertView * alertView=[[UIAlertView alloc] initWithTitle:@"Error" message:[result objectForKey:@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        [alertView release];
    }
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


@end