//
//  Application.h
//  SalesCatalog
//
//  Created by Thirumalasetti Karthik on 28/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Build, Category, ScreenShot;

@interface Application : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * info;
@property (nonatomic, retain) NSData * icon;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) Category *category;
@property (nonatomic, retain) NSSet *screen_shots;
@property (nonatomic, retain) NSSet *builds;
@property (nonatomic, retain) NSString * icon_url;
@property (nonatomic, retain) NSNumber * icon_downloaded;
@property (nonatomic, retain) NSDate * updated_date;
@end

@interface Application (CoreDataGeneratedAccessors)

- (void)addScreen_shotsObject:(ScreenShot *)value;
- (void)removeScreen_shotsObject:(ScreenShot *)value;
- (void)addScreen_shots:(NSSet *)values;
- (void)removeScreen_shots:(NSSet *)values;

- (void)addBuildsObject:(Build *)value;
- (void)removeBuildsObject:(Build *)value;
- (void)addBuilds:(NSSet *)values;
- (void)removeBuilds:(NSSet *)values;

-(void)updateWithDict:(NSDictionary * )dict category:(Category *)category;
-(void)retriveApplicationIconWithDataReciever:(id)dataReciever;

@end