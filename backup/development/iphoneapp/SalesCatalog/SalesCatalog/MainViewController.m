//
//  MainViewController.m
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 15/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "ServerInterface.h"
#import "MainViewController.h"
#import "LoginViewController.h"


@implementation MainViewController

@synthesize loginViewController=_loginViewController;

-(void)awakeFromNib{

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewWillAppear:(BOOL)animated{
        [self.selectedViewController viewWillAppear:animated];
}

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{    
    LoginViewController * tmp=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    self.loginViewController=tmp;
    [tmp release];
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    ServerInterface * interface=[ServerInterface sharedInstance];
    if(!interface.loggedIn){
        [self presentModalViewController:self.loginViewController animated:YES];
    }
    else{
        //fetch applications
    }
    [self.selectedViewController viewDidAppear:animated];
    [super viewDidAppear:animated];
}

- (void)viewDidUnload
{
    self.loginViewController=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)dealloc{
    if(_loginViewController){
        [_loginViewController release];
    }
    [super dealloc];
}

@end