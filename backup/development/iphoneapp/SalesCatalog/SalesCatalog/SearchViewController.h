//
//  Class.h
//  sales_lab
//
//  Created by Thirumalasetti Karthik on 16/11/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Application.h"
#import "ApplicationServeProtocol.h"

@interface SearchViewController : UIViewController<UISearchBarDelegate, UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, ApplicationServeProtocol>{
    NSArray * applications;
    Application * currentSelectedApplication;
    UIViewController * detailedViewController;
    UITableView *tableView;
    UISearchBar *searchBar;
}


@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (retain, nonatomic) NSArray * applications;
@property (retain, nonatomic) Application * currentSelectedApplication;
@property (retain, nonatomic) UIViewController * detailedViewController;

@end
